#!/bin/bash

if (( $# > 0 ))
then
    hostname=$1
else
    echo "Please supply a hostname or IP address as an argument" 1>&2
    exit 1
fi

ssh -o StrictHostKeyChecking=no  -i ~/.ssh/BrianTingKey.pem ec2-user@$hostname '
sudo yum -y install httpd
if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
    sudo systemctl start httpd
else
    exit 1
fi


sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>WOW</h1>
<img src=\"https://media.giphy.com/media/5VKbvrjxpVJCM/giphy.gif\">
_END_"
'
